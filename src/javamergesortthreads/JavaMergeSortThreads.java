/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamergesortthreads;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
/**
 *
 * @author kasprus
 */
public class JavaMergeSortThreads {
    private static AtomicInteger numberOfCreatedThreads = new AtomicInteger(0);
    private static AtomicInteger maximumNumberOfCreatedThreads = new AtomicInteger(16);
    public static <T extends Comparable<T>>void mergeSort(T tab[]){
        T pom[]=(T [])new Comparable [tab.length];
        parallelSort(tab, pom, 0, tab.length-1);
    
}
    private static <T extends Comparable <T>> void merge(T tab[], T pom[], int beginIndex, int mid, int lastIndex){
        int a=beginIndex;
        while(a<=lastIndex){
            pom[a]=tab[a];
            ++a;
        }
        a=mid+1;
        int wsp=beginIndex;
        while(beginIndex<=mid||a<=lastIndex){
            if(a>lastIndex)tab[wsp++]=pom[beginIndex++];
            else if(beginIndex>mid)tab[wsp++]=pom[a++];
            else{
                if(pom[beginIndex].compareTo(pom[a])>0)tab[wsp++]=pom[beginIndex++];
                else{
                    tab[wsp++]=pom[a++];
                }
            }
        }
    }
    private static <T extends Comparable <T>>void normalSort(T tab[], T pom[], int beginIndex, int lastIndex){
        int mid=(beginIndex+lastIndex)/2;
        
        if(mid-beginIndex>0){
            normalSort(tab, pom, beginIndex, mid);
        }
        if(lastIndex-mid>1){
            normalSort(tab, pom, mid+1, lastIndex);
        }
        merge(tab, pom, beginIndex, mid, lastIndex);
}
    
    private static<T extends Comparable <T>> void parallelSort(T tab[], T pom[], int beginIndex, int lastIndex){
        int mid=(beginIndex+lastIndex)/2;
        Thread t1 = null, t2 = null;
        if(mid-beginIndex>0){
            if(numberOfCreatedThreads.get()<maximumNumberOfCreatedThreads.get()){
                t1=new Thread(new Runnable(){
                @Override
                public void run(){
                    numberOfCreatedThreads.incrementAndGet();
                    parallelSort(tab, pom, beginIndex, mid);
                    
                }
            });
                t1.setPriority(Thread.MAX_PRIORITY);
                t1.start();
                
            }
            else{
                normalSort(tab, pom, beginIndex, mid);
            }
           /* t1=new Thread(new Runnable(){
                @Override
                public void run(){
                    numberOfCreatedThreads.incrementAndGet();
                    parallelSort(tab, pom, beginIndex, mid);
                    
                }
            });*/
        }
        if(lastIndex-mid>1){
            if(numberOfCreatedThreads.get()<maximumNumberOfCreatedThreads.get()){
           t2=new Thread(new Runnable(){
                @Override
                public void run(){
                    numberOfCreatedThreads.incrementAndGet();
                    parallelSort(tab, pom, mid+1, lastIndex);
                }
            });
           t2.setPriority(Thread.MAX_PRIORITY);
           t2.start();
        }
            else{
                normalSort(tab,pom,mid+1,lastIndex);
            }
        }
        try{
            if(t1!=null){
                t1.join();
                numberOfCreatedThreads.decrementAndGet();
            }
        }
        catch(InterruptedException e){
            System.out.println("Dupa");
        }
        try{
            if(t2!=null){
                t2.join();
                numberOfCreatedThreads.decrementAndGet();
            }
        }
        catch(InterruptedException e){
            System.out.println("Dupa");
        }
        merge(tab, pom, beginIndex, mid, lastIndex);
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random rand = new Random();
        // TODO code application logic here
        Integer tab1[]=new Integer[10000000];
        Integer tab2[]=new Integer[10000000];
        for (int i=0; i<tab1.length; ++i){
            tab2[i]=rand.nextInt();
            tab1[i]=tab2[i];
        }
        System.out.println("zaczynam sortowanie...");
        long start=System.nanoTime();
        mergeSort(tab1);
        long koniec=System.nanoTime();
        System.out.println("Czas sortowania mergsortem z watkami wyniosl" +(koniec-start));
        System.out.println("sortowanie wbudowanie trwa");
        start=System.nanoTime();
        Arrays.sort(tab2);
        koniec=System.nanoTime();
        System.out.println("trwalo "+(koniec-start));
       /* for(Integer k:tab){
            System.out.println(k);
        }*/
    }
    
}
